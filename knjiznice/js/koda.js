
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var generirani = ["a339dd09-9d1f-4508-9ec4-0cc7d3e732e8", "073ead6c-e89e-4275-91d4-4af1d9e81af4", "303c3105-4c59-48f8-83e7-b9b6de02b7e1"];
console.log(generirani);

var itm2 = [];
var teza = [];
var visina = [];


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function detailKlic(i){
  console.log(i);
  $("#detail").html("");
  var xdxd = "";
  xdxd += "Visina: " + visina[i] / 100 + "m";
  xdxd += " | Teza: " + teza[i] + "kg";
  $("#detail").html(xdxd);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function generirajVnose(stPacienta){
  var sessionId = getSessionId();

  if(stPacienta == 1){
    $.ajaxSetup({
      headers: {"Ehr-Session": sessionId}
    });
    var merilec = "medicinska sestra Micka";
    var podatki = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": "2017-05-05T14:14",
      "vital_signs/height_length/any_event/body_height_length": "180",
      "vital_signs/body_weight/any_event/body_weight": "56",
      "vital_signs/body_temperature/any_event/temperature|magnitude": "37",
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      "vital_signs/blood_pressure/any_event/systolic": "118",
      "vital_signs/blood_pressure/any_event/diastolic": "92",
      "vital_signs/indirect_oximetry:0/spo2|numerator": "98"
    };
    var parametri = {
      ehrId: generirani[0],
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: merilec
    };
    $.ajax({
      url: baseUrl + "/composition?" + $.param(parametri),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      success: function(res){
        console.log("Vpisani podatki za pac st. " + stPacienta);
      },
      error: function(err){
        console.log("Napak pri vnosu podatkov za pac st. " + stPacienta);
      }
    })
  }else if(stPacienta == 2){
    $.ajaxSetup({
      headers: {"Ehr-Session": sessionId}
    });
    var podatki = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": "2017-05-05T14:14",
      "vital_signs/height_length/any_event/body_height_length": "183",
      "vital_signs/body_weight/any_event/body_weight": "73",
      "vital_signs/body_temperature/any_event/temperature|magnitude": "37",
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      "vital_signs/blood_pressure/any_event/systolic": "118",
      "vital_signs/blood_pressure/any_event/diastolic": "92",
      "vital_signs/indirect_oximetry:0/spo2|numerator": "98"
    };
    var parametri = {
      ehrId: generirani[1],
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: merilec
    };
    $.ajax({
      url: baseUrl + "/composition?" + $.param(parametri),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      success: function(res){
        console.log("Vpisani podatki za pac st. " + stPacienta);
      },
      error: function(err){
        console.log("Napak pri vnosu podatkov za pac st. " + stPacienta);
      }
    })
  }else{


    $.ajaxSetup({
      headers: {"Ehr-Session": sessionId}
    });
    var podatki = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": "2017-05-05T14:14",
      "vital_signs/height_length/any_event/body_height_length": "183",
      "vital_signs/body_weight/any_event/body_weight": "150",
      "vital_signs/body_temperature/any_event/temperature|magnitude": "37",
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      "vital_signs/blood_pressure/any_event/systolic": "118",
      "vital_signs/blood_pressure/any_event/diastolic": "92",
      "vital_signs/indirect_oximetry:0/spo2|numerator": "98"
    };
    var parametri = {
      ehrId: generirani[2],
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: merilec
    };
    $.ajax({
      url: baseUrl + "/composition?" + $.param(parametri),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      success: function(res){
        console.log("Vpisani podatki za pac st. " + stPacienta);
      },
      error: function(err){
        console.log("Napak pri vnosu podatkov za pac st. " + stPacienta);
      }
    })

  }
}
function generirajPodatke(stPacienta) {
  ehrId = "";
  var ime = "";
  var priimek = "";
  var datum = "";
  var sessionId = getSessionId();

  $.ajaxSetup({
    headers: {"Ehr-Session": sessionId}
  });

  if(stPacienta == 1){
    // zelo suh
    ime = "Boris";
    priimek = "Suhec";
    datum = "1993-04-08T13:03";

  }else if(stPacienta == 2){
    // dobra forma
    ime = 'Miha';
    priimek = 'Telovadec';
    datum = '1994-07-04T18:34';
  }else{
    // zelo debel
    ime = "Rado";
    priimek = "Jedec";
    datum = '1990-01-06T09:24';
  }
  console.log('klic za pacienta st ' + stPacienta);
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      success: function (data){
          var ehrId = data.ehrId;
          var partyData = {
              firstNames: ime,
              lastNames: priimek,
              dateOfBirth: datum,
              partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
          };
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party){
                  if(party.action == 'CREATE'){
                      console.log('EhrId za pacienta ' + stPacienta + ': ' + ehrId);
                      generirani[stPacienta - 1] = ehrId;
                  }
              },
              error: function(err) {
                      console.log('Error nekusn');
              }
          })
      }
  });



  return ehrId;
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
$(document).ready(function(){
    var dodajNovegaUporabnika = function(){

        //$("#preberiEhrVnos").val("Omfg");


        var sessionId = getSessionId();
        var ime = document.getElementById('imeVnos').value;
        var priimek = document.getElementById('priimekVnos').value;
        var datum = document.getElementById('datumrojstvaVnos').value;

        //console.log(ime +" " + priimek + " " + datum);
        if(!ime || !priimek || !datum || ime.trim().length == 0 ||
        priimek.trim().length == 0 || datum.trim().length == 0){
            //console.log("zahtevani podatki napacni");
            $("#sporociloVnos").html("<b>Prosim vnesite zahtevane podatke.</b>");
        }else{
            $.ajaxSetup({
                headers: {"Ehr-Session": sessionId}
	    	});
	    	console.log(sessionId);
	    	$.ajax({
	    	    url: baseUrl + "/ehr",
	    	    type: 'POST',
	    	    success: function (data){
	    	        var ehrId = data.ehrId;
	    	        var partyData = {
	    	            firstNames: ime,
	    	            lastNames: priimek,
	    	            dateOfBirth: datum,
	    	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	    	        };
	    	        $.ajax({
	    	            url: baseUrl + "/demographics/party",
	    	            type: 'POST',
	    	            contentType: 'application/json',
	    	            data: JSON.stringify(partyData),
	    	            success: function (party){
	    	                if(party.action == 'CREATE'){
	    	                    $("#sporociloVnos").html("<b>Uspešno kreiran EHR: " + ehrId +".</b>");
	    	                    //console.log(party);

	    	                    $("#preberiEhrVnos").val(ehrId);
	    	                }
	    	            },
	    	            error: function(err) {
	    	                $("#sporociloVnos").html("<b>Napaka: " + JSON.parse(err.responseText).userMessage + ".</b>")
	    	            }
	    	        })
	    	    }
	    	});
        }

    }

    document.getElementById("vnosButton").addEventListener('click', dodajNovegaUporabnika);
    var pridobiPodatke = function(){
        var sessionId = getSessionId();
        var ehrId = document.getElementById('preberiEhrVnos').value;
        console.log(ehrId);

        if(!ehrId || ehrId.trim().length == 0){
            $("#sporociloPridobi").html("<b>Prosim vnesite zahtevan podatek!</b>");
        }else{
            $.ajax({
                url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
                type: 'GET',
                headers: {"Ehr-Session": sessionId},

                success: function(data){
                    var party = data.party;
                    $("#sporociloPridobi").html("<b>Bolnik " + party.firstNames + " "+ party.lastNames + ", ki se je rodil " + party.dateOfBirth + ".</b>");
                },
                error: function(err){
                    $("#sporociloPridobi").html("<b>Napaka: "+ JSON.parse(err.responseText).userMessage + "!</b>");
                }
            })
        }
    }
    document.getElementById("pridobiPodatke").addEventListener('click', pridobiPodatke);

    var dodajMeritve = function(){
      var sessionId = getSessionId();

      var ehrId = document.getElementById('ehrMeritve').value;
      var datumInUra = document.getElementById('datumMeritve').value;
      var telesnaVisina = document.getElementById("visinaMeritve").value;
      var teza = document.getElementById("tezaMeritve").value;
      var temp = document.getElementById("tempMeritve").value;
      var sistTlak = document.getElementById("stlakMeritve").value;
      var distTlak = document.getElementById("dtlakMeritve").value;
      var nasicenost = document.getElementById("nasicenostMeritev").value;
      var merilec = document.getElementById("merilecMeritve").value;

      //console.log(ehrId + " " + datumInUra + " " + telesnaVisina + " " + teza + " " + temp + " " + sistTlak + " " + distTlak + " " + nasicenost + " " + merilec);
      if(!ehrId || ehrId.trim().length == 0){
          //console.log("napaka");
          $("#sporociloMeritve").html("<b>Prosim vnesite zahtevane podatke.</b>");
      }else{
        $.ajaxSetup({
          headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
  		    "ctx/language": "en",
  		    "ctx/territory": "SI",
  		    "ctx/time": datumInUra,
  		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
  		    "vital_signs/body_weight/any_event/body_weight": teza,
  		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temp,
  		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
  		    "vital_signs/blood_pressure/any_event/systolic": sistTlak,
  		    "vital_signs/blood_pressure/any_event/diastolic": distTlak,
  		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
		    };
        var parametri = {
          ehrId: ehrId,
          templateId: 'Vital Signs',
          format: 'FLAT',
          committer: merilec
        };
        $.ajax({
          url: baseUrl + "/composition?" + $.param(parametri),
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(podatki),
          success: function(res){
            $("#sporociloMeritve").html("<b>" + res.meta.href + ".</b>");
          },
          error: function(err){
            $("#sporociloMeritve").html("<b>Napaka: " + JSON.parse(err.responseText).userMessage + ".</b>")
          }
        })
      }

    }
    document.getElementById("meritve").addEventListener('click', dodajMeritve);

    // pridobi podatke
    var pridobiBaza = function(){
      var sessionId = getSessionId();
      var ehrId = $("#ehrResult").val();

      //console.log(ehrId);
      teza = [];
      visina = [];
      var cas = [];
      var konec = false;
      var niPodatkov = false;
      $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function(data){
          $("#sporociloBaza").html("Pridobivam.");
          $("#rezultati").html("");
          $(".graf").html("");
          $("#map").html("");
          $("#detail").html("");
          var party = data.party;
          $.ajax({
            url: baseUrl + "/view/" + ehrId + "/weight",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function(x){
              $("#sporociloBaza").html("<b>Uspesno!</b>");
              if(x.length != 0){
                for(var i in x){
                  //console.log(x[i].time + " || " + x[i].weight);
                  cas.push(x[i].time);
                  teza.push(x[i].weight);

                }
              }else{
                $("#sporociloBaza").html("<b>Napaka: Ni podatkov!</b>");
                niPodatkov = true;
              }

            },
            error: function(err){
              $("#sporociloBaza").html("<b>Napaka: Ni podatkov!</b>");
            }
          });
          $.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function(x){
              $("#sporociloBaza").html("<b>Uspesno!</b>");
              if(x.length != 0){
                for(var i in x){
                  //console.log(x[i].height + " cm");
                  //console.log(x[i].time + " " + x[i].weight);
                  //cas.push(x[i].time);
                  //teza.push(x[i].weight);
                  visina.push(x[i].height);
                }

                konec = true;
              }else{
                $("#sporociloBaza").html("<b>Napaka: Ni podatkov!</b>");
                konec = true;
                niPodatkov = true;
              }

            },
            error: function(err){
              $("#sporociloBaza").html("<b>Napaka: Ni podatkov!</b>");
            }
          });
          //console.log(data);

        },
        error: function(err){
          $("#sporociloBaza").html("<b>Napaka: " + JSON.parse(err.responseText).userMessage + "</b>");
        }
      })
      var jeKonec = function(){
        if(konec == true){
          console.log("je konec!");
          if(niPodatkov == false){
            var resu = "";
            itm2 = [];
            for(var i in cas){
              //console.log("Cas: " + cas[i] + " || teza: " + teza[i]+ " || visina: " + visina[i]);
              resu += "<b>Cas: " + cas[i] + " || teza: " + teza[i]+ " || visina: " + visina[i] + "</b><br>";

              var zadnjaTeza = teza[i];
              var zadnjaVisina = visina[i] / 100;
              var itm = zadnjaTeza / Math.pow(zadnjaVisina, 2);
              itm2[i] = Math.floor(itm);
            }
            //$("#rezultati").html(resu);
            $("#rezultati").html("<b> Izmerjeni ITM: ");
            console.log("PRVA PRIDOBLJENA MERITEV: ");
            console.log("Cas: " + cas[cas.length - 1] + " || teza: " + teza[teza.length - 1]+ " || visina: " + visina[visina.length - 1]);
            var zadnjaTeza = teza[teza.length - 1];
            var zadnjaVisina = visina[visina.length - 1] / 100;
            var itm = zadnjaTeza / Math.pow(zadnjaVisina, 2);
            console.log("Itm: " + itm);


            d3.select(".graf").selectAll("div").data(itm2).enter().append("div")
            .style("width", function(sirina) {
               return sirina * 8 + "px"
             }).style("background-color", "lightgreen").style('cursor', 'pointer').style("color", "white").style("border", "1px solid red").style("text-align", "right").text(function(sirina) {
               return sirina;
             }).attr('id', function(d, i){
               return "id" + i;
             }).attr('onClick', function(d, i){
               return 'detailKlic(' + i + ')';
             });

             setTimeout(function(){
               for(var i in itm2){
                 var id = "id" + i;
                 console.log(id);
                 document.getElementById(id).addEventListener('click', function(){
                   console.log(id);

                 })
               }
             }, 1000);
            var x = $(".graf").html();
            console.log(x);
            x += "<br>";
            x += "Prva pridobljena meritev ITM: <b>" + itm2[0] + "</b>. ";
            var iskanje = "";
            if(itm2[0] > 25){
              // meritev prevelika
              x += "<br>";
              x += "Izmerjen ITM je bil <b>visok</b>. Priporočamo obisk fitnesa.";
              x += "<br>";
              x += "<button id='najdiFitnes' class='btnvnos'>Navodila za fitnes</button>";
              iskanje = "fit";
            }else if(itm2[0] > 18){
              x += "<br>";
              x += "Izmerjen ITM je bil <b>normalen</b>. Ste v dobri formi, vendar telovadba nikoli ne škodi.";
              x += "<br>";
              x += "<button id='najdiFitnes' class='btnvnos'>Navodila za fitnes</button>";
              iskanje = "fit";
            }else{
              x += "<br>";
              x += "Izmerjen ITM je bil <b>nizek</b>. Priporočamo obisk restavracije.";
              x += "<br>";
              x += "<button id='najduRestavracijo' class='btnvnos'>Navodila za restavracijo</button>";

              iskanje = "res";
            }


            $(".graf").html(x);

            if(iskanje == "fit"){
              document.getElementById("najdiFitnes").addEventListener('click', fitnesTime);
            }else{
              document.getElementById("najduRestavracijo").addEventListener('click', restavracijaTime);
            }


          }else{
            console.log("Ni podatkov!");
            $("#rezultati").html("");
          }
        }else{
          setTimeout(jeKonec, 1000);
        }
      }
      jeKonec();

      console.log("wow");

    }

    document.getElementById("pridobiButton").addEventListener('click', pridobiBaza);



    // ZA FITNESS
    var fitnesTime = function(){
      //console.log("iscem fitnes");
      // pocisti #rezultati in .graf, nado dodaj noter google maps slikco k kaze kje je ta fitnes
      //var prej = $("#mainthing").html();
      //$("#mainthing").html("");
      $(".graf").html("");
      $("#rezultati").html("");
      $("#detail").html("");
      var uluru = {lat: 46.060022, lng: 14.495735};
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: uluru
      });
      var marker = new google.maps.Marker({
        position: uluru,
        map: map
      });
      /*var zdej = $("#mainthing").html;
      zdej += "<br>";
      zdej += "<button id='povrni' class='btnvnos'>Pojdi nazaj</button>";

      $("#mainthing").html(zdej);
      document.getElementById("povrni").addEventListener('click', function(){
        $("#mainthing").html(prej);
      })*/


      //$("#rezultati").html("");
    }
    //document.getElementByid("najdiFitnes").addEventListener('click', fitnesTime);

    // GENERIRANJE TREH PACIENTOV

    var novaGeneracija = function(){
      //document.getElementById('ponovnoGeneriraj').html("GENERIRAM");
      $("#ponovnoGeneriraj").html("GENERIRAM");

      $("#normalenKlic").html("POCAKAJTE");
      $("#suhecKlic").html("POCAKAJTE");
      $("#debeliKlic").html("POCAKAJTE");
      var ehrSuhca = generirajPodatke(1);
      var ehrNormalnega = generirajPodatke(2);
      var ehrDebelega = generirajPodatke(3);

      setTimeout(function(){
        console.log(generirani);
        generirajVnose(1);
        generirajVnose(2);
        generirajVnose(3);
        $("#ponovnoGeneriraj").html("GENERIRANI!");
        $("#normalenKlic").html("Normalen ITM");
        $("#suhecKlic").html("Nizek ITM");
        $("#debeliKlic").html("Visok ITM");
        setTimeout(function(){
          $("#ponovnoGeneriraj").html("PONOVNO GENERIRAJ 3 PACIENTE");
        }, 1000);
      }, 2000);

    }
    document.getElementById('ponovnoGeneriraj').addEventListener('click', novaGeneracija);
    document.getElementById('ponovnoGenerirajDefault').addEventListener('click', function(){
      novaGeneracija();
    });


    var generiraniMenu = function(){
      document.getElementById('menuElementi').classList.toggle('pokazi');
    }

    document.getElementById('generiraniButton').addEventListener('click', generiraniMenu);


    document.getElementById('suhecKlic').addEventListener('click', function(){
      document.getElementById('preberiEhrVnos').value = generirani[0];
      document.getElementById('ehrResult').value = generirani[0];
      document.getElementById('ehrMeritve').value = generirani[0];
    });
    document.getElementById('normalenKlic').addEventListener('click', function(){
      document.getElementById('preberiEhrVnos').value = generirani[1];
      document.getElementById('ehrResult').value = generirani[1];
      document.getElementById('ehrMeritve').value = generirani[1];
    });
    document.getElementById('debeliKlic').addEventListener('click', function(){
      document.getElementById('preberiEhrVnos').value = generirani[2];
      document.getElementById('ehrResult').value = generirani[2];
      document.getElementById('ehrMeritve').value = generirani[2];
    });







    // ZA restavracijo

    var restavracijaTime = function(){
      console.log("iscem restavracijo");

      $(".graf").html("");
      $("#rezultati").html("");
      $("#detail").html("");
      var temp = "";
      temp += "<button id='izbiraRestavracije' class='btnvnos'>Prikazi mozne restavracije</button>"
      $("#rezultati").html(temp);


      var izbiraRest = function(){
        temp = "<button id='izbiraRestavracijeOff' class='btnvnos'>Skrij mozne restavracije</button><br><br /> \
                  <table> \
                    <tr><td><button id='mcdonalds' class='btnvnos'>McDonald's</button> Restavracija hitre prehrane: </td></tr> \
                    <tr><td><button id='burgerking' class='btnvnos'>Burger King</button> Restavracija hitre prehrane: </td></tr> \
                    <tr><td><button id='subway' class='btnvnos'>Subway</button> Restavracija z sendviči: </td></tr> \
                  </table>"
        $("#rezultati").html(temp);

        document.getElementById('izbiraRestavracijeOff').addEventListener('click', function(){
          $("#rezultati").html("<button id='izbiraRestavracije' class='btnvnos'>Prikazi mozne restavracije</button>");
          document.getElementById('izbiraRestavracije').addEventListener('click', izbiraRest);
        })

        // mcdonalds
        document.getElementById('mcdonalds').addEventListener('click', function(){
          $(".graf").html("");
          $("#rezultati").html("");
          $("#detail").html("");

          var uluru = {lat: 46.051997, lng: 14.504882};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: uluru
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: map
          });

          var txt = $('.graf').html();
          txt += "<button id='nazaj' class='btnvnos'>Nazaj na izbiro restavracij</button>";
          $('.graf').html(txt);

          document.getElementById('nazaj').addEventListener('click', function(){
            $(".graf").html("");
            $("#rezultati").html("");
            $("#map").html("");
            $("#detail").html("");
            var temp = "";
            temp += "<button id='izbiraRestavracije' class='btnvnos'>Prikazi mozne restavracije</button>"
            $("#rezultati").html(temp);

            document.getElementById('izbiraRestavracije').addEventListener('click', izbiraRest);
          })
        })

        // burger king
        document.getElementById('burgerking').addEventListener('click', function(){
          $(".graf").html("");
          $("#rezultati").html("");
          $("#detail").html("");

          var uluru = {lat: 46.068306, lng: 14.545637};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: uluru
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: map
          });

          var txt = $('.graf').html();
          txt += "<button id='nazaj' class='btnvnos'>Nazaj na izbiro restavracij</button>";
          $('.graf').html(txt);

          document.getElementById('nazaj').addEventListener('click', function(){
            $(".graf").html("");
            $("#rezultati").html("");
            $("#map").html("");
            $("#detail").html("");
            var temp = "";
            temp += "<button id='izbiraRestavracije' class='btnvnos'>Prikazi mozne restavracije</button>"
            $("#rezultati").html(temp);

            document.getElementById('izbiraRestavracije').addEventListener('click', izbiraRest);
          })
        })

        // subway
        document.getElementById('subway').addEventListener('click', function(){
          $(".graf").html("");
          $("#rezultati").html("");
          $("#detail").html("");

          var uluru = {lat: 46.075069, lng: 14.511277};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 19,
            center: uluru
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: map
          });
          var txt = $('.graf').html();
          txt += "<button id='nazaj' class='btnvnos'>Nazaj na izbiro restavracij</button>";
          $('.graf').html(txt);

          document.getElementById('nazaj').addEventListener('click', function(){
            $(".graf").html("");
            $("#rezultati").html("");
            $("#map").html("");
            $("#detail").html("");
            var temp = "";
            temp += "<button id='izbiraRestavracije' class='btnvnos'>Prikazi mozne restavracije</button>"
            $("#rezultati").html(temp);

            document.getElementById('izbiraRestavracije').addEventListener('click', izbiraRest);
          })
        })


      }
      document.getElementById("izbiraRestavracije").addEventListener('click', izbiraRest);

    }

    // vnos privzetih


    document.getElementById("vnesiDefault").addEventListener('click', function(){
      document.getElementById("tempMeritve").value = '36.50';
      document.getElementById("stlakMeritve").value = '118';
    document.getElementById("dtlakMeritve").value = '92';
      document.getElementById("nasicenostMeritev").value = '98';
      document.getElementById("merilecMeritve").value = 'Blank';
    })
})

// suhec: 48d8d586-d566-40e3-80cf-9fcd576927ad
// normalen/debeli: 54a6dd28-e357-4054-b363-c272b8b59449
